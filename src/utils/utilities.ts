
import { v4 as uuid } from "uuid";
import path from "path";

export function uuidFilenameTransform(filename = "") {
    const fileExtension = path.extname(filename);
  
    return `${uuid()}${fileExtension}`;
}