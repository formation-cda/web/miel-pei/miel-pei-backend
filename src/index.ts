import { createConnection } from 'typeorm';
import { ApolloServer } from 'apollo-server';
import { buildSchema } from 'type-graphql';
import { graphqlUploadExpress } from "graphql-upload";
import { UserResolver } from './resolvers/userResolver';
import { ProductResolver } from './resolvers/poductsResolver';
import {ExploitationResolver} from './resolvers/exploitationResolver'
const { env } = require('./config/config'); 
import Auth from './utils/Auth' 
import express from 'express'
import { OrderResolver } from './resolvers/orderResolver';
const app = express()
const path = require('path');
app.use(graphqlUploadExpress({ maxFileSize: 1000000000, maxFiles: 10 }));
//Serves all the request which includes /images in the url from Images folder
app.use('/files', express.static(path.join(__dirname + '/assets')))
app.listen(env.PIC_PORT,  () => console.log('Pictures served on port 5000!'));

async function runServer() {
  const connection = await createConnection();
  const schema = await buildSchema({
    resolvers: [
      UserResolver,
      ProductResolver,
      ExploitationResolver,
      OrderResolver
    ]
  });

  const server = new ApolloServer({ 
    schema,  
    context: Auth
 });
  const port = env.PORT;
  await server.listen(`${port}`);

  console.log('🚀 Server started at port ::'+`${port} 🚀`);
}

runServer();