import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, ManyToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { OrderStatus } from './order_status';
import { User } from './users';
import { Product } from './products';
import { Order } from './orders';

@ObjectType()
@Entity("order_details")
export class OrderDetails extends BaseEntity {

  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Field(() => Date)
  @Column()
  qty!: number;
   
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
  
  @Field(() => Order)
  @ManyToOne(() => Order, order => order.orderDetails, { onDelete: 'CASCADE' })
  @JoinColumn()
  order!: Order;

  @Field(() => Product)
  @ManyToOne(() => Product, product => product.orderDetails, { onDelete: 'CASCADE' })
  @JoinColumn()
  product!: Product;

}

