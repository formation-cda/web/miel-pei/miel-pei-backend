import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { Product } from './products';
import { Order } from './orders';
import { Exploitation } from './exploitations';
import { Role } from './role';

@ObjectType()
@Entity("users")
export class User extends BaseEntity {

  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Field({ nullable: true })
  @Column({ default: null })
  lastName: String;

  @Field(() => String)
  @Column()
  firstName: String;

  @Field(() => String)
  @Column({ default: null })
  avatar: String;

  @Field(() => String)
  @Column()
  email: String;

  @Field(() => Boolean)
  @Column({ default: false })
  active: boolean;
  
  @Field({ nullable: true })
  @Column({ default: null })
  billingAddsress: String;

  @Field({ nullable: true })
  @Column({ default: null })
  deliveryAddsress: String;
  
  @Field({ nullable: true })
  @Column({ default: null })
  resetToken: String;
  @Field(() => String)

  @Field({ nullable: true })
  @Column({ default: null })
  expireToken: Date;
   
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
    
  @Field(() => Role)
  @ManyToOne(() => Role, role => role.user, { onDelete: 'CASCADE' })
  @JoinColumn()
  userRole: Role;
  
  @OneToMany(() => Exploitation, exploitation => exploitation.producer)
  exploitation: Exploitation[];

  @OneToMany(() => Order, order => order.client)
  order: Order[];

  @Field(() => String)
  @Column({transformer: new EncryptionTransformer({
    key: 'e41c966f21f9e1577802463f8924e6a3fe3e9751f201304213b2f845d8841d61',
    algorithm: 'aes-256-cbc',
    ivLength: 16,
    iv: 'ff5ac19190424b1d88f9419ef949ae56'
  })})
  password: String;
}

