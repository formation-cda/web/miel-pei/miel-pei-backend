import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne, ManyToOne, JoinTable, ManyToMany, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { User } from './users';
import { Exploitation } from './exploitations';
import { OrderDetails } from './order_details';

@ObjectType()
@Entity("products")
export class Product extends BaseEntity {

  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Field(() => String)
  @Column()
  name: String;

  @Field(() => String)
  @Column()
  pictures: String;

  @Field(() => Number)
  @Column()
  price: Number;

  @Field(() => Number)
  @Column({ default: 0 })
  qtyInStock: Number;

  @Field(() => Number)
  @Column({ default: 0 })
  qtyInCart: Number;
 
  @Field({ nullable: true })
  @Column('longtext',{ default: null })
  description: String;
   
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Field(() => Exploitation)
  @ManyToOne(() => Exploitation, exploitation => exploitation.product, { onDelete: 'CASCADE' })
  @JoinColumn()
  exploitation: Exploitation;

  @OneToMany(() => OrderDetails, order_details => order_details.product)
  orderDetails!: OrderDetails[];

}

