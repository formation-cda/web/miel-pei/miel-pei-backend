import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, ManyToMany, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { OrderStatus } from './order_status';
import { User } from './users';
import { Product } from './products';
import { OrderDetails } from './order_details';

@ObjectType()
@Entity("orders")
export class Order extends BaseEntity {

  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => Date)
  @Column()
  dateOrder: Date;

  @Field(() => String)
  @Column()
  numOrder: String;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;  
  
  @Field(() => User)
  @ManyToOne(() => User, user => user.order, { onDelete: 'CASCADE' })
  @JoinColumn()
  client: User;

  @Field(() => OrderStatus)
  @ManyToOne(() => OrderStatus, order_status => order_status.order, { onDelete: 'CASCADE' })
  @JoinColumn()
  orderStatus: OrderStatus;

  @OneToMany(() => OrderDetails, order_details => order_details.order)
  orderDetails: OrderDetails[];
   
}

