import { Resolver, Query, Arg, Mutation, Ctx } from "type-graphql";
import { CreateUserInput, UpdateUserInput, LoginUserInput, SignupUserInput } from "../types/userInputType";
import { User } from "../models/entities/users";
import { UserResponse, LoginResponse, CommonResponse, SignInResponse } from "../types/ResponseType";
import { Role } from "../models/entities/role";
const jwt = require('jsonwebtoken');
const { env } = require('../config/config');  
@Resolver()
export class UserResolver {

  @Query(() => String)
  test() {
    return "Server is running well ...";
  }

  @Query(() => [User])
  async users() {
    const users = await User.find();
    // console.log("Users : ",users)
    return users
  }

  @Query(() => User)
  async user(@Arg("id") id: string) {
    return await User.findOne({ where: { id }});
  }

  @Query(() => UserResponse)
  async getRole(
    @Arg("id") id: string,
    @Ctx() context: any
    ) {
    if(context.isAuth) {
      const user = await User.findOne({ 
        where: { id },
        relations: ['userRole']
      });
      return {data: user, success: true};
    }      
    return {success: false, message:"Utilisateur non autorisé"}
  }

  @Mutation(() => UserResponse)
  async addUser(
    @Arg("data") {email,lastName,firstName, password}: CreateUserInput,    
    @Ctx() context: any
    ) {
      const me = await User.findOne({
        where: { id: context.userId },
        relations: ['userRole']
      });
      const role = await Role.findOne({ name: "admin" });
      if(context.isAuth && me && me.userRole === role) {
        const user = await User.create({email,lastName,firstName, password});
        console.log("Data : ",{email,lastName,firstName, password})
        await user.save();
        if (!user) return {success: false, message:"Echec d'ajout d'utilidsateur, veuillez réessayer"}
        return {data: user,success: true};
      }      
      return {success: false, message:"Utilisateur non autorisé"}
  }
  
  @Mutation(() => UserResponse, {nullable:true})
  async updateUser(
    @Arg("id") id: string, @Arg("data") data: UpdateUserInput,
    @Ctx() context: any
    )  {

      if(context.isAuth) {
        const user = await User.findOne({ where: { id }});
        // if (!user) {throw new Error(`The user with id: ${id} does not exist!`);}
        if (!user) return {success: false, message:`L'utilisateur portant l'id: ${id} n'existe pas!`}
    
        Object.assign(user, data);
        const newUser = await user.save();    
        if (!newUser) return {success: false, message:"Echec de mise à jourd'utilisateur, veuillez réessayer"}
        console.log(newUser)
        return {data: newUser, success: true};
      }      
      return {success: false, message:"Utilisateur non autorisé"}
  }

  @Mutation(() => CommonResponse)
  async deleteUser(
    @Arg("id") id: string,    
    @Ctx() context: any
    ) {
      const me = await User.findOne({
        where: { id: context.userId },
        relations: ['userRole']
      });
      const role = await Role.findOne({ name: "admin" });
      if(context.isAuth && me && me.userRole === role) {
      const user = await User.findOne({ where: { id }});

      // if (!user) { throw new Error(`The user with id: ${id} does not exist!`);}
      if (!user) return {success: false, message:`L'utilisateur portant l'id: ${id} n'existe pas!`}

      await user.remove();
      return {success: true};
    }
    return {success: false, message:"Utilisateur non autorisé"}
  }

  @Mutation(() => SignInResponse)
  async signupUser (
    // @Arg("data") data: SignupUserInput
      @Arg("email") email:String,
      @Arg("lastName") lastName:String,
      @Arg("firstName") firstName:String,
      @Arg("password") password:String,
      @Arg("role") role:String
    ){
    // Object.assign(data, {isAdmin: false});
    
    const userRole = await Role.findOne({ where: { name: role }});
    const user = await User.create({email,lastName,firstName, password, userRole});
    await user.save();
    if (!user) return {success: false, message:"Echec d'inscription, veuillez réessayer"}
    const token = jwt.sign({user}, env.JWT_SECRET)
    return {token, user, success: true};
  }

  @Mutation(() => LoginResponse, {nullable:true})
  async loginUser (
    @Arg("email") mail: String,
    @Arg("password") pwd: String
    )
  {
    const { email, password } = {email: mail, password:pwd};

    const user = await User.findOne({ 
      where: { email }, 
      relations: ['userRole']
    });
    // if (!user) throw new Error('Unable to Login');
    if (!user) return {success: false, message:"Utilisateur non enregistré"}
    console.log("Userrr : ", user)
    const isMatch = (password == user.password)
    // if (!isMatch) throw new Error('Unable to Login');
    if (!isMatch) return {success: false, message:"Mot de passe incorrect"}
    const userId = user.id
    const token = jwt.sign({userId}, env.JWT_SECRET)
    return {token, user, success: true};
  }

  @Mutation(() => CommonResponse, {nullable:true})
  async logout ()
  {
    //do something here
    return {success: true};
  }
  
}
