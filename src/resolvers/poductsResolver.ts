import { Resolver, Query, Arg, Mutation, Ctx } from "type-graphql";
import {GraphQLUpload} from "apollo-server-express"
import {createWriteStream} from 'fs'
import { CreateUserInput, UpdateUserInput, LoginUserInput, SignupUserInput } from "../types/userInputType";
import { User } from "../models/entities/users";
import { LoginResponse, CommonResponse, SignInResponse, ProductResponse, ProductDetailsResponse } from "../types/ResponseType";
import { Product } from "../models/entities/products";
import { Exploitation } from "../models/entities/exploitations";
import { Upload } from "../types/Upload";
import { uuidFilenameTransform } from "../utils/utilities";
import { Role } from "../models/entities/role";
const jwt = require('jsonwebtoken');
const { env } = require('../config/config');  
@Resolver()
export class ProductResolver {

  @Query(() => String)
  test() {
    return "Server is running well ...";
  }

  @Query(() => ProductResponse)
  async products(    
    @Ctx() context: any
  ) {
      const products = await Product.find({
          order: {
              updated_at: "DESC"
          },
          relations:['exploitation']
      });

      if (!products || products.length == 0) return {success: false, message:`Aucun produit!`}
      return {data: products, success: true}
  }

  @Query(() => ProductResponse)
  async allMyproducts(    
    @Ctx() context: any
  ) {
    if(context.isAuth) {
        const me = await User.findOne({ where: { id: context.userId }});
        const products = await Product.find({
            where: {userId: me },
            order: {
                updated_at: "DESC"
            },
            relations:['exploitation']
        });

        if (!products || products.length == 0) return {success: false, message:`Aucun produit!`}
        console.log("Products : ", products)
        return {data: products, success: true}
    }  
    return {success: false, message:"Utilisateur non autorisé"}
  }

  @Query(() => ProductResponse)
  async allProductsByProducer(       
    @Arg("id") id: string, 
    @Ctx() context: any
  ) {
    const me = await User.findOne({ where: { id: context.userId }});
    const producer = await User.findOne({ id });
    const role = await Role.findOne({ name: "admin" });
    if(context.isAuth && me && me.userRole === role) {
        const products = await Product.find({
            where: {userId: producer },
            order: {
                updated_at: "DESC"
            },
            relations:['exploitation']
        });

        if (!products || products.length == 0) return {success: false, message:`Aucun produit!`}
        console.log("Products : ", products)
        return {data: products, success: true}
    }  
    return {success: false, message:"Utilisateur non autorisé"}
  }

  @Query(() => ProductDetailsResponse)
  async product(
      @Arg("id") id: string,      
      @Ctx() context: any
    ) {
        const product = await Product.findOne({ 
          where: { id },
          relations:['exploitation']
        })  

      console.log("PRODD :",product)
      return {data:product, success: true}
  }  

  @Mutation(() => ProductResponse)
  async addProduct (
      @Arg("name") name:String,
      @Arg("price") price:String,
      @Arg("qty_in_stock") qtyInStock:String,    
      @Arg("pictures", ()=> GraphQLUpload) {
        createReadStream,
        filename
      }:Upload,
      @Arg("description") description:String,
      @Arg("exploitation") exploitation_id:String,
      @Ctx() context: any
    ){
        console.log(context)
        if(context.isAuth) {
          const newFileName = uuidFilenameTransform(filename)
          const picture = `/pictures/products/${newFileName}`
          console.log("Test add explotation")
          return new Promise(async (resolve, reject) =>
              createReadStream()
              .pipe(createWriteStream(process.cwd()+`/src/assets/pictures/products/${newFileName}`))
              .on("finish", async () => {
                const me = await User.findOne({ where: { id: context.userId }});
                const exploitation = await Exploitation.findOne({ where: { id: exploitation_id }});
                const product = await Product.create({
                    name,
                    price: Number(qtyInStock),
                    pictures:picture, 
                    description,
                    exploitation,
                    qtyInStock: Number(qtyInStock)
                });
                await product.save();
                if (!product) return {success: false, message:"Echec d'ajout de produit"}
                const token = jwt.sign({product}, env.JWT_SECRET)
                resolve({token, product, success: true, message: "Produit enregistré avec succès"})
              })
              .on("error", (error) => {
                  console.log("Error :",error)
                  reject({success: true, message: "Echec d'ajout du produit"})
                })
          )
      
      }
      return {success: false, message:"Utilisateur non autorisé"}
  }

  @Mutation(() => CommonResponse)
  async deleteProduct(
    @Arg("id") id: string,    
    @Ctx() context: any
    ) {
    if(context.isAuth) {
      const me = await User.findOne({ where: { id: context.userId }});
      const product = await Product.findOne({ where: { id }});

      if (!product) return {success: false, message:`Le produit portant l'id: ${id} n'existe pas!`}

      await product.remove();
      return {success: true};
    }
    return {success: false, message:"Utilisateur non autorisé"}
  }

}
