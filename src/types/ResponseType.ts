import { ObjectType ,Field, ArgsType } from 'type-graphql';
import { Exploitation } from '../models/entities/exploitations';
import { Product } from '../models/entities/products';
import { User } from '../models/entities/users';

@ObjectType()
export class CommonResponse {
  @Field({ nullable: true })
  success?: boolean;

  @Field({ nullable: true })
  message?: String;
}

@ObjectType()
export class UserResponse extends CommonResponse {
  @Field({ nullable: true })
  data: User
}

@ObjectType()
export class ProductResponse extends CommonResponse {
  @Field((_type) => Product,{ nullable: true })
  data: Product[]
}

@ObjectType()
export class ProductDetailsResponse extends CommonResponse {
  @Field((_type) => Product,{ nullable: true })
  data: Product
}


@ObjectType()
export class ExploitationResponse extends CommonResponse {
  @Field((_type) => Exploitation,{ nullable: true })
  data: Exploitation[]
}


// @ObjectType()
// export class MultipleProductResponse extends CommonResponse {
//   @Field({ nullable: true })
//   data: Product[]
// }

@ObjectType()
export class LoginResponse extends CommonResponse {
  @Field({ nullable: true })
  token?: String;
  @Field({ nullable: true })
  user?: User
}

@ObjectType()
export class SignInResponse extends CommonResponse {
  @Field({ nullable: true })
  token: String;
  @Field({ nullable: true })
  user?: User;
}
