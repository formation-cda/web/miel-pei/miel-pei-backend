## Miel péi frontEnd

***
### Préréquis
Avoir les outils suivant installé
- **Nodejs**
- **npm** ou **yarn**
- **mysql**
- **git**

### Clone Repository

```git clone https://gitlab.com/formation-cda/miel-pei/miel-pei-frontend.git```

### Installation

```yarn```

ou

```yarn install```

ou 

```npm install```

### Configuration
Créér un fichier ***.env.development et .env.production*** à la racine du projet en suivant le modèle de ***.env.development.exemple et .env.production.env***

### Lancement de l'application
```yarn start```

ou 

```npm start```

## N.B:
Les valeurs de des variables: 
- **REACT_APP_BACKEND_PIC_PORT** (en mode production) **REACT_APP_BACKEND_PIC_PORT_DEV** (en mode développement) doivent correspondre à celui de la variable **PIC_PORT** du backend

- **REACT_APP_BACKEND_PORT** (en mode production) **REACT_APP_BACKEND_PORT_DEV** (en mode développement) doivent correspondre à celui de la variable **PORT** du backend
